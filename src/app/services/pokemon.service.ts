import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PokemonsModel } from '../model/pokemons.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private endPoint = 'https://pokeapi.co/api/v2/pokemon?offset=__offset__';

  constructor(
    private httpClient: HttpClient
  ) { }

  public getAll(offset: number) : Observable<PokemonsModel|null> {
    return this.httpClient.get<PokemonsModel>(
      this.endPoint.replace('__offset__', offset.toString())
    );
  }
}
