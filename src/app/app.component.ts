import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { PokemonsModel } from './model/pokemons.model';
import { PokemonService } from './services/pokemon.service';
import { merge } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  public model: PokemonsModel;

  // tu auras besoin de la taille totale pour que ça fonctionne correctement.
  public total: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  constructor(
    private pokemonService: PokemonService
  ) {} 
  
  ngAfterViewInit() {
    console.log(this.paginator);
    
    merge(this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          return this.pokemonService.getAll(this.paginator.pageIndex * this.paginator.pageSize);
        })
      ).subscribe(data => {
          this.model = data;
          this.total = data.count;
      });
  }
}
