export interface PokemonsModel {
    count: number;
    next: string;
    previous: string;
    results: Result[];
}

export interface Result {
    url: string;
    name: string;
}